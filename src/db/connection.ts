import { Sequelize } from 'sequelize-typescript';
import { Car } from './models/car.model';
import 'dotenv/config';
/*
  Главный файл для подключения к базе, экспортируется в main.ts
  Вместо обычного пакета sequelize во всем приложении используется sequelize-typescript.
  Доп. информация: https://www.npmjs.com/package/sequelize-typescript
 */

const connection = new Sequelize(process.env.DB_URL!, {
  // Указываем модели, которые хотим инициализировать
  models: [Car],
  logging: false,
});

export default connection;
