import { Column, DataType, Length, Model, Table } from 'sequelize-typescript';

/*
    Одна модель - Одна таблица в базе.
    Для удобства используем декораторы.
    Так же тут указываем нужные нам параметры для валидации и сообщения в случае несоответсвий.
    Дополнительно по созданию моделей при помощи декораторов:
    https://www.npmjs.com/package/sequelize-typescript#model-definition
 */

@Table({
  timestamps: false,
  tableName: 'cars',
})
export class Car extends Model {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  declare name: string;

  @Length({ msg: 'Описание должно содержать как минимум 15 символов', min: 10 })
  @Column({
    type: DataType.TEXT,
    allowNull: true,
  })
  declare description: string;

  @Length({ msg: 'Год должен состоять из 4 цифр', min: 4, max: 4 })
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  declare year: number;
}
