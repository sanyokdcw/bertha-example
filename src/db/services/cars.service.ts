import { Car } from '../models/car.model';

/*
 Сервисы используются для работы непосредсвенно с моделями.
 Вся логика, связанная с моделями - прописывается тут.
 В данном сервисе представлена базовая логика CRUD.
 */

export const getAll = async (): Promise<Car[]> => {
  return await Car.findAll();
};

export const getOne = async (id: number): Promise<Car | null> => {
  return await Car.findByPk(id);
};

export const store = async (body: Partial<Car>): Promise<Car> => {
  return await Car.create(body);
};

export const update = async (
  id: number,
  body: Partial<Car>,
): Promise<Car | { message: string }> => {
  let result;
  const car: Car | null = await Car.findByPk(id);
  if (car != null) {
    result = await car.update(body, { where: { id } });
  } else {
    result = { message: `Машины с id ${id} не существует` };
  }
  return result;
};

export const destroy = async (
  id: number,
): Promise<{ isDeleted: number; message: string }> => {
  const result: number = await Car.destroy({ where: { id } });
  return {
    isDeleted: result,
    message: result
      ? 'Машина удалена успешно'
      : `Машины с id ${id} не существует`,
  };
};
