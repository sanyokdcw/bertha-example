import 'reflect-metadata';
import express, { Express } from 'express';
import connection from "./db/connection";
import routes from './api/routes/index';
import 'dotenv/config';
import cors from 'cors';
/*
  Входной файл приложения.
  Импортируем роутеры, middleware, проводим подключение к базе.
 */

const app: Express = express();
const port: string = process.env.PORT!;

app.use(express.json());
app.use(cors())
app.use('/', routes);

const start = async (): Promise<void> => {
  try {
    await connection.sync({ alter: true });
    app.listen(port, () => {
      console.log(`Server started on port ${port}`);
    });
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

void start();
