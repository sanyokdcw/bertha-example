import { Request, Response } from 'express';
import * as carsService from '../../db/services/cars.service';
import { StoreCarInterface } from '../interfaces/storeCar.interface';
import { UpdateCarInterface } from '../interfaces/updateCar.interface';

/*
  Контроллер ловит запросы с роутера и служит только для того,
  чтобы отправить входные данные в сервис и вернуть ответ.
 */

export const index = async (req: Request, res: Response) => {
  return res.status(200).json(await carsService.getAll());
};

export const show = async (req: Request<{ id: string }>, res: Response) => {
  // Params всегда приходят как string, поэтому нужно преобразовывать id к типу number
  const id = Number(req.params.id);
  return res.status(200).json(await carsService.getOne(id));
};

export const store = async (req: Request<StoreCarInterface>, res: Response) => {
  return res.status(201).json(await carsService.store({ ...req.body }));
};

export const update = async (
  req: Request<{ id: string }, UpdateCarInterface>,
  res: Response,
) => {
  const id = Number(req.params.id);
  return res.status(200).json(await carsService.update(id, { ...req.body }));
};

export const destroy = async (req: Request<{ id: string }>, res: Response) => {
  const id = Number(req.params.id);
  return res.status(200).json(await carsService.destroy(id));
};
