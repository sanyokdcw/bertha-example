import { NextFunction, Request, Response } from 'express';
import { Car } from '../../db/models/car.model';

/*
    Middleware для валидации полей.
    Реализовано через метод validate().
    Запрос проверяется еще до входа в сервис,
    в случае несоответсвий в ответ отправляется 400 Bad Request и описание ошибок.
    Доп. информация: https://www.npmjs.com/package/sequelize-typescript#model-validation
 */

export const carsValidate = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  if (!Object.keys(req.body).length) {
    next();
  } else {
    const validationErrors: any = {};
    let car = Car.build(req.body);
    try {
      await car.validate();
      next();
    } catch (errors: any) {
      errors.errors.map((err: any) => {
        validationErrors[err.path] = err.message;
      });
      res.status(400).json(validationErrors);
    }
  }
};
