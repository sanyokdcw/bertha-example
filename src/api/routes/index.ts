import { Router } from 'express';
import carsRouter from './cars.router';

/*
  Главный файл роутеров.
  Импортируем все роутеры в этот файл и экспортируем всё в main.ts.
 */

const router: Router = Router();

router.use('/cars', carsRouter);

export default router;
