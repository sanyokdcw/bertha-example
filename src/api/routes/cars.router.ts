import { Request, Response, Router } from 'express';
import * as carsController from '../controllers/cars.controller';
import { carsValidate } from '../middlewares/cars.validate';

/*
  Пример роутинга CRUD.
  Принимаются запросы и обрабатываются контроллером.
  Если нужно указать middleware для конкретного роутера или эндпойнта, делаем это здесь.
 */

const carsRouter = Router();

// Middleware для валидации
carsRouter.use('/', carsValidate);

carsRouter.get('/', carsController.index);

carsRouter.get('/:id', carsController.show);

carsRouter.post('/', carsController.store);

carsRouter.put('/:id', carsController.update);

carsRouter.delete('/:id', carsController.destroy);

export default carsRouter;
