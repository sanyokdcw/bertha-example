/*
    Интерфейс для входных данных при обновлении данных машины.
    Необязательные поля отмечаются "?".
 */

export interface UpdateCarInterface {
  name?: string;
  description?: string;
  year?: number;
}
