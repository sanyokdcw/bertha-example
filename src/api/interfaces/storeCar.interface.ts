/*
    Интерфейс для входных данных при создании машины.
 */

export interface StoreCarInterface {
  name: string;
  description?: string;
  year: number;
}
